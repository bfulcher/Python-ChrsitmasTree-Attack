Both scripts in this repo perform a 'Christmas Tree' attack on a given host.


Single threaded version useage: christmas_tree.py -t x.x.x.x

Multi threaded version useage: christmas_tree_multithread.py -t x.x.x.x -w int(x)

TODO:

Would like to have the worker processes check back in with the main thread while running

Test on beefier machine, not a VM